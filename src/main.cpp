#include <ZCross.hpp>
#include <iostream>
#include <boost/units/systems/si/codata/atomic-nuclear_constants.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/electron_constants.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/cmath.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <random>
#include <stdexcept>
	
using namespace dfpe;
using namespace std;
using namespace boost::units;
using namespace boost::units::si::constants::codata;
using namespace boost::accumulators;

template <class T>
tuple<T, T, T> cross(T x1, T y1, T z1, T x2, T y2, T z2, T u)
{
	T x3 =  (y1 * z2 - y2 * z1) / u;
	T y3 =  (x2 * z1 - x1 * z2) / u;
	T z3 =  (x1 * y2 - x2 * y1) / u;
	
	return make_tuple(x3, y3, z3);
} 

template <class T>
T dot(T x1, T y1, T z1, T x2, T y2, T z2, T u)
{
	return (x1 * x2 + y1 * y2 + z1 * z2) / u;
} 

template <class T>
T norm(T x, T y, T z)
{
	return hypot(hypot(x, y), z);
} 


template <class T>
tuple<T, T, T> rotate(T x, T y, T z, T axisX, T axisY, T axisZ, T u, quantity<si::plane_angle> angle)
{
	
	T axisNorm = norm<T>(axisX, axisY, axisZ);
	
	axisX /= axisNorm / u;
	axisY /= axisNorm / u;
	axisZ /= axisNorm / u;
	
	T resX;
	T resY;
	T resZ;
	
	resX += x * cos(angle);
	resY += y * cos(angle);
	resZ += z * cos(angle);
	
	T cX, cY, cZ;
	
	tie(cX, cY, cZ) = cross<T>(axisX, axisY, axisZ, x, y, z, u);
	
	resX += cX * sin(angle);
	resY += cY * sin(angle);
	resZ += cZ * sin(angle);
	
	
	T dt = dot<T>(axisX, axisY, axisZ, x, y, z, u);
	
	resX += axisX * dt / u * (1 - cos(angle));
	resY += axisY * dt / u * (1 - cos(angle));
	resZ += axisZ * dt / u * (1 - cos(angle));
	
	return make_tuple(resX, resY, resZ);
} 


int main()
{
	
	quantity<si::area>   m2(1. * si::meter * si::meter);
    quantity<si::energy> eV(1. * si::volt  * si::constants::codata::e);
    quantity<si::velocity> cm_us(1. * si::centi * si::meter / (si::micro * si::second));


    /// derived dimension for electric field in SI units : L M T⁻³ A⁻¹
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension,			boost::units::static_rational<1>>,
			boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<1>>,
			boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-3>>,
			boost::units::dim<boost::units::current_base_dimension,   		boost::units::static_rational<-1>>
    >>::type     electric_field_dimension;
    
    /// derived dimension for electric field in SI units : L⁴ M T⁻³ A⁻¹
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension,	        boost::units::static_rational<4>>,
            boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<1>>,
            boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-3>>,
            boost::units::dim<boost::units::current_base_dimension,   		boost::units::static_rational<-1>>
    >>::type     reduced_electric_field_dimension;
    
    /// derived dimension for number density in SI units : L⁻³
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension,    boost::units::static_rational<-3>>
    >>::type     number_density_dimension;


    typedef boost::units::unit<electric_field_dimension, boost::units::si::system>         electric_field;
    typedef boost::units::unit<reduced_electric_field_dimension, boost::units::si::system> reduced_electric_field;
    typedef boost::units::unit<number_density_dimension, boost::units::si::system>         number_density;


	quantity<electric_field> kV_cm(1. * si::kilo * si::volt  / ( si::centi * si::meter));
	quantity<reduced_electric_field> townsend(1e-21 * si::volt  * si::meter * si::meter);
	
	
	default_random_engine generator;
	
	unsigned int now = std::chrono::system_clock::now().time_since_epoch().count();
	generator.seed(now);
	
	uniform_real_distribution<double> distribution(0.0,1.0);

	ZCross zcross;

    zcross.loadDatabase("Biagi");

    ProcessFilter filter;
    filter.addFilterByDatabaseId("Biagi");
    filter.addFilterByGroupId("Ar");
    filter.addFilterByProcessCollisionType(ProcessCollisionType::ELASTIC);

	cout << "Loading table:" << endl;
    const IntScatteringTable table =  zcross.getIntScatteringTables(filter)[0];
    
	cout << "[" << table.getId() << "]" << endl;
	cout << "  " << table.getProcess().getShortDescription() << endl;
	cout << "  " << table.getProcess().getReaction().toString(PrintMode::PRETTY) << endl;
	cout << "  " << "Comments: " << endl;
	cout << "  " << table.getProcess().getComments() << endl;
	cout << endl;
	
	cout << std::right
		 << setw(16) << "ENERGY"
		 << "   "
		 << setw(16) << "CROSS SECTION"
		 << endl;
		 
	for (const auto &pair : table.getTable())
	{
		cout << std::right
			 << setw(16) << (double) (pair.first / eV)  << " eV"
			 << setw(16) << (double) (pair.second / m2) << " m²"
			 << endl;
	}
    
	cout << endl;
	
	
	
	// Loschmidt constant
	quantity<number_density> density = 2.6867811e25 / (si::meter * si::meter * si::meter);
	quantity<reduced_electric_field> reducedFieldZ = 2. * townsend;
	quantity<electric_field> fieldZ = reducedFieldZ * density;
	
	accumulator_set<double, features<tag::mean,tag::variance>> acc;
	
	int events = 10;
	
	long interactions = 1000000;
	
	for (int eventId = 0; eventId < events; eventId ++)
	{
		
		cout << "Event " << eventId << endl;
		
		quantity<si::time> currentTime;
		
		quantity<si::length> positionX;
		quantity<si::length> positionY;
		quantity<si::length> positionZ;
		
		quantity<si::velocity> velocityX;
		quantity<si::velocity> velocityY;
		quantity<si::velocity> velocityZ;
		quantity<si::velocity> velocity;
		
		quantity<si::energy>   energy;
		
		long currentInteraction = 0;
		
		quantity<si::frequency> trialFrequency(15. * si::tera * si::hertz);
		
		
		double realCollisions = 0.;
		double nullCollisions = 0.;
		double failCollisions = 0.;
		
		while (currentInteraction < interactions)
		{
			quantity<si::dimensionless> rnd1 = distribution(generator);
			quantity<si::time> deltaTime = -log(1. - rnd1) / trialFrequency;
			
			quantity<si::velocity> newVelocityX = velocityX;
			quantity<si::velocity> newVelocityY = velocityY;
			quantity<si::velocity> newVelocityZ = velocityZ + e * fieldZ / m_e * deltaTime;
			
			quantity<si::velocity> newVelocity  = norm(newVelocityX, newVelocityY, newVelocityZ);
			quantity<si::energy>   newEnergy    = 0.5 * m_e * newVelocity * newVelocity;
			
			quantity<si::length> newPositionX = positionX + velocityX * deltaTime;
			quantity<si::length> newPositionY = positionY + velocityY * deltaTime;
			quantity<si::length> newPositionZ = positionZ + velocityZ * deltaTime + 0.5 * e * fieldZ / m_e * deltaTime * deltaTime;

			quantity<si::area> crossSection = table.getInterpoledValue(newEnergy, OutOfTableMode::BOUNDARY_VALUE,  OutOfTableMode::BOUNDARY_VALUE);
			
			quantity<si::frequency> realFrequency = newVelocity * density * crossSection;
			
			if (trialFrequency >= realFrequency)
			{                              
					quantity<si::dimensionless> ratioReal = realFrequency / trialFrequency;
					quantity<si::dimensionless> rnd2 = distribution(generator);

					currentTime += deltaTime;
					
					positionX = newPositionX;
					positionY = newPositionY;
					positionZ = newPositionZ;
					
					velocityX = newVelocityX;
					velocityY = newVelocityY;
					velocityZ = newVelocityZ;
					velocity  = newVelocity;

					energy	  = newEnergy;
					
					if (rnd2 < ratioReal) // Checking if it is a not-null collision
					{
						
						// Setting new direction
						
						quantity<si::mass> bulletMass = m_e;
						quantity<si::mass> targetMass = Molecule("Ar").getMass();
						quantity<si::mass> totalMass  = bulletMass + targetMass;

						quantity<si::energy> bulletEnergyInitial = energy;

						quantity<si::dimensionless> rnd3 = distribution(generator);
						quantity<si::plane_angle>   phi = rnd3 * quantity<si::plane_angle>(2. * M_PI * si::radians);

						quantity<si::dimensionless> rnd4 = distribution(generator);
						quantity<si::dimensionless> epsilon = bulletEnergyInitial / E_h;
						
						quantity<si::plane_angle> theta = acos(quantity<si::dimensionless>(1. - 2. * rnd4 / (1. + 8. * epsilon * (1. - rnd4))));
						//quantity<si::plane_angle> theta = rnd4 * quantity<si::plane_angle>(M_PI * si::radians);
						
						quantity<si::energy> lostEnergy = 2. * bulletMass * targetMass / (totalMass * totalMass) * (1. - cos(theta)) * bulletEnergyInitial;
						//quantity<si::energy> lostEnergy;

						//{
							//QtySiEnergy threshold = table.getEnergyLowerBound();
							//QtySiDimensionless partA = -targetMass / totalMass * threshold / bulletEnergyInitial;
							//QtySiDimensionless partB1 = 2. * targetMass * bulletMass / (totalMass * totalMass);
							//QtySiDimensionless partB2 = max(one - totalMass / targetMass * threshold / bulletEnergyInitial, QtySiDimensionless());
							//QtySiDimensionless partB3 = sqrt(partB2) * cos(theta) - one;

							//lostEnergy = -(partA + partB1 * partB3) * bulletEnergyInitial;
						//}        
						
						assert(lostEnergy >= quantity<si::energy>());
						assert(bulletEnergyInitial >= lostEnergy);

						quantity<si::energy>   postEnergy   = bulletEnergyInitial - lostEnergy;
						quantity<si::velocity> postVelocity = sqrt(2. * postEnergy / bulletMass);

						if (newVelocity > quantity<si::velocity>())
						{
							 // Creating a perpendicular vector
							 // Let's try with X
					
							  auto [perX, perY, perZ] = cross(velocityX, velocityY, velocityZ, quantity<si::velocity>(1. * si::meter / si::second), quantity<si::velocity>(), quantity<si::velocity>(), quantity<si::velocity>(1. * si::meter / si::second));
							  
							  assert( dot(velocityX, velocityY, velocityZ, perX, perY, perZ, quantity<si::velocity>(1. * si::meter / si::second)) == quantity<si::velocity>());
							  if ( norm(perX, perY, perZ)  < quantity<si::velocity>(1e-5 * si::meter / si::second)) 
								throw runtime_error("We was unable to find a parallel vector"); 
								
							  quantity<si::velocity> postVelocityX, postVelocityY, postVelocityZ;
							  
							  tie(postVelocityX, postVelocityY, postVelocityZ) = rotate<quantity<si::velocity>>(velocityX, velocityY, velocityZ, perX, perY, perZ, quantity<si::velocity>(1. * si::meter / si::second), theta);
							  tie(postVelocityX, postVelocityY, postVelocityZ) = rotate<quantity<si::velocity>>(postVelocityX, postVelocityY, postVelocityZ, velocityX, velocityY, velocityZ, quantity<si::velocity>(1. * si::meter / si::second), phi);
							  
							  assert(abs(norm(velocityX, velocityY, velocityZ) - norm(postVelocityX, postVelocityY, postVelocityZ)) < quantity<si::velocity>(1e-5 * si::meter / si::second) );
							  
							  postVelocityX *= postVelocity / velocity;
							  postVelocityY *= postVelocity / velocity;
							  postVelocityZ *= postVelocity / velocity;
							  
							  //cout << "--------------------" << endl;
							  //cout << "collision " << currentInteraction << " theta: " << (double) (theta / quantity<si::plane_angle>(M_PI * si::radians)) * 180 << " deg, phi: " << (double) (phi / quantity<si::plane_angle>(M_PI * si::radians)) * 180 << " deg, cs " << (double) (crossSection / m2) << " m2, real freq: " << realFrequency << endl;  
							  //cout << "before " << velocityX << ", " << velocityY << ", " << velocityZ << " ( energy: " << (double) (energy/eV) << " eV)" << endl;
							  //cout << "after  " << postVelocityX << ", " << postVelocityY << ", " << postVelocityZ << " ( energy: " << (double) (postEnergy/eV) << " eV)" << endl;
							  // Ok, this is the new velocity after the collision
							  velocityX = postVelocityX;
							  velocityY = postVelocityY;
							  velocityZ = postVelocityZ;
							  velocity  = postVelocity;
							  energy    = postEnergy;
							 
						}
						else
						{
							velocityX = quantity<si::velocity>();
							velocityY = quantity<si::velocity>();
							velocityZ = quantity<si::velocity>();
							velocity  = quantity<si::velocity>();
							energy    = quantity<si::energy>();
						}
						
						
						currentInteraction++;
						
						realCollisions += 1.;
					}
					else
					{
						nullCollisions += 1.;
					}
				
			}
			else
			{
				failCollisions += 1.;
				
				cout << "Trial frequency: " << trialFrequency << endl;
				cout << "Real  frequency: " << realFrequency << endl;
				
				throw runtime_error("Trial frequency was lower than the real frequency");
				
			}		
		}
		
		// Registering the velocity
		acc((double)(positionZ / currentTime / cm_us));
	}
	
	quantity<si::velocity> velocityMean  = mean(acc) * cm_us;
	quantity<si::velocity> velocityStdev = sqrt(variance(acc)) * cm_us;
	
	cout << std::right
		<< std::setw(10) << "run id"
		<< std::setw(10) << "events"
		<< std::setw(10) << "particles"
		<< std::setw(14) << "interactions"
		<< std::setw(20) << "field [kV/cm]"
		<< std::setw(20) << "reduced field [Td]"
		<< std::setw(30) << "drift velocity mean [cm/us]"
		<< std::setw(30) << "drift velocity stdev [cm/us]"
		<< endl;
		
	wcout << std::right
		 << std::setw(10) << 0
		 << std::setw(10) << events
		 << std::setw(10) << 1
		 << std::setw(14) << interactions
		 << std::setw(20) << (double) (fieldZ             / kV_cm )
		 << std::setw(20) << (double) (reducedFieldZ      / townsend )
		 << std::setw(30) << (double) (velocityMean       / cm_us )
		 << std::setw(30) << (double) (velocityStdev      / cm_us )
		 << endl;
	
}
